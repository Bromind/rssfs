rssfs
=====

This repository hosts a simple filesystem for rss feeds. 


## Change diary

 - April 12th, 2022: I realized that this README has not been uploaded for a while. The filesystem is now much more comfortable to use. We can provide a config file using "-c" option (or ~/.config/rssfs by default) to specify which feeds should be followed. Also, there is a special file "update" that updates the feeds when something is written into that file. Finally, I decided to add this "diary-style" changelog to the README: as I intend it to remain quite small, I think we can afford to have a more friendly changelog, instead of a cold list of changes.
 - old: This is work in progress. It barely works, but the configuration is static. You should set the mountpoint and the feeds you want directly in the source code. By default, it reads only my website feed (http://vassor.org/rss.xml) and the mountpoint is `/tmp/rssfs`. Updates are not performed: feeds are fetched at startup and not refreshed after.

