extern crate chrono;
extern crate clap;
extern crate dirs;
extern crate fuser;
extern crate libc;
extern crate minreq;
extern crate rss;
extern crate time;
extern crate toml;
#[macro_use]
extern crate serde_derive;
use chrono::DateTime;
use fuser::mount2;
use fuser::{
    FileAttr, FileType, Filesystem, KernelConfig, ReplyAttr, ReplyData, ReplyDirectory, ReplyEmpty,
    ReplyEntry, ReplyOpen, ReplyWrite, Request,
};
use rss::{Channel, Item};
use std::ffi::OsStr;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::{Duration, SystemTime};

use libc::{EISDIR, ENOENT, ENOTDIR, EPERM};

use clap::{Command, Arg};

struct Rssfs {
    uid: u32,
    gid: u32,
    channels: Vec<ChanBox>,
}

#[derive(Clone)]
struct ChanBox {
    title: String,
    url: String,
    items: Vec<(u32, Item)>,
    _ttl: u32,
    _last_update: u32,
    item_counter: u32,
}

impl ChanBox {
    fn items(self) -> Vec<(u32, Item)> {
        self.items
    }

    fn title(&self) -> &str {
        &self.title
    }

    fn from_url(url: &str) -> Self {
        let response = minreq::get(url).send().unwrap();
        let buf = BufReader::new(response.as_bytes());
        let chan = Channel::read_from(buf).unwrap();
        let mut i = 0;
        let items = chan
            .items()
            .iter()
            .map(|item| {
                let j = i;
                i += 1;
                (j, item.clone())
            })
            .collect();
        ChanBox {
            title: chan.title().to_string(),
            url: url.to_string(),
            items,
            _ttl: 0,
            _last_update: 0,
            item_counter: i,
        }
    }

    fn update(&mut self) {
        let current_items = self
            .items
            .clone()
            .into_iter()
            .map(|(_, i)| i)
            .collect::<Vec<_>>();
        let response = minreq::get(&self.url).send().unwrap();
        let buf = BufReader::new(response.as_bytes());
        let chan = Channel::read_from(buf).unwrap();
        chan.items().iter().for_each(|item| {
            if !current_items.contains(item) {
                self.items.push((self.item_counter, item.clone()));
                self.item_counter += 1
            }
        })
    }
}

#[derive(Debug, Deserialize)]
struct Config {
    mountpoint: String,
    channels: Vec<String>,
}

// Inode is divided into 2 parts: the 32 LSB (channel index) and the 32 MSB (item index)
// Channel index 0 is not used
// Channel index 1 is reserved for the content of root directory of the file system:
//   item index 0 is used for the `root` directory
//   item index 1 is used for the `update` file
// Channel indexes in [2, ...] are used for the content of the channels:
//   item index 0 is used for the directory
//   item indexes `n` in [1, ...] are used for item `n-1`
//
//                  Item Index  Chan Index
// /tmp/rssfs/      0           1
// ├── update       1           1
// ├── Channel 0    0           2
// │   ├── Item 0   1           2
// │   ├── Item 1   2           2
// │   ├── Item 2   3           2
// │   ├── Item 3   4           2
// │   └── Item 4   5           2
// └── Channel 1    0           3
//     ├── Item 0   1           3
//     ├── Item 1   2           3
//     ├── Item 2   3           3
//     ├── Item 3   4           3
//     └── Item 4   5           3
//
//
fn indexes_from_inode(ino: u64) -> Option<(u32, Option<u32>)> {
    let item_index: u32 = (ino >> 32) as u32;
    let channel_index: u32 = (ino as u32) & u32::max_value();
    if channel_index <= 1 {
        None
    } else {
        if item_index == 0 {
            Some((channel_index - 2, None))
        } else {
            Some((channel_index - 2, Some(item_index - 1)))
        }
    }
}

fn inode_is_root(ino: u64) -> bool {
    (ino >> 32 == 0) && (ino as u32 & u32::max_value() == 1)
}

fn inode_is_update(ino: u64) -> bool {
    (ino >> 32 == 1) && (ino as u32 & u32::max_value() == 1)
}

fn inode_of_update() -> u64 {
    (1 << 32) + 1
}

fn inode_from_indexes(chan_index: u32, item_index: Option<u32>) -> u64 {
    let mut ret: u64 = chan_index as u64 + 2;
    if let Some(i) = item_index {
        ret += ((i + 1) as u64) << 32;
    }
    ret
}

fn epoch_from_date(s: String) -> Option<i64> {
    DateTime::parse_from_rfc2822(&s)
        .ok()
        .map(|dt| dt.timestamp())
}

fn string_of_item(item: &Item) -> String {
    item.title().unwrap_or("").to_string()
        + "\t"
        + item.pub_date().unwrap_or("")
        + "\n"
        + item.link().unwrap_or("")
        + "\n\n"
        + "Description: \n"
        + item.description().unwrap_or("")
        + "\n\n"
        + item.content().unwrap_or("")
        + "\n"
}

impl Rssfs {
    fn inode_from_channel_name(&self, chan_title: &str) -> Option<u64> {
        let mut i = 0;
        for c in &self.channels {
            if c.title() == chan_title {
                return Some(inode_from_indexes(i as u32, None));
            }
            i += 1;
        }
        return None;
    }

    fn inode_from_item_name(&self, chan_index: u32, item_title: &str) -> Option<u64> {
        if let Some((i, _)) = self.channels[chan_index as usize]
            .items
            .iter()
            .filter(|(_, item)| item.title() == Some(item_title))
            .next()
        {
            return Some(inode_from_indexes(chan_index, Some(*i)));
        }
        return None;
    }

    fn item_at_index(&self, chan_index: u32, item_index: u32) -> Option<&Item> {
        if let Some((_, item)) = self.channels[chan_index as usize]
            .items
            .iter()
            .filter(|(i, _)| *i == item_index)
            .next()
        {
            return Some(item);
        }
        return None;
    }

    fn attr_of_inode(&self, ino: u64) -> Result<FileAttr, i32> {
        let attr = if let Some((chan_index, item_index)) = indexes_from_inode(ino) {
            if chan_index as usize >= self.channels.len() {
                return Err(ENOTDIR);
            }

            if let Some(i) = item_index {
                if let Some(item) = self.item_at_index(chan_index, i) {
                    let date = item
                        .pub_date()
                        .map(|s| epoch_from_date(s.to_string()).unwrap_or(0))
                        .unwrap_or(0);
                    let time = SystemTime::UNIX_EPOCH + Duration::from_secs(date as u64);
                    FileAttr {
                        ino: ino,
                        size: string_of_item(item).into_bytes().len() as u64,
                        blocks: 0,
                        blksize: 0,
                        atime: time,
                        mtime: time,
                        ctime: time,
                        crtime: time,
                        kind: FileType::RegularFile,
                        perm: 0o444,
                        nlink: 0,
                        uid: self.uid,
                        gid: self.gid,
                        rdev: 0,
                        flags: 0,
                    }
                } else {
                    // No item found at index item_index
                    return Err(ENOENT);
                }
            } else {
                // No item_index
                FileAttr {
                    ino: ino,
                    size: 0,
                    blocks: 0,
                    blksize: 0,
                    atime: SystemTime::UNIX_EPOCH,
                    mtime: SystemTime::UNIX_EPOCH,
                    ctime: SystemTime::UNIX_EPOCH,
                    crtime: SystemTime::UNIX_EPOCH,
                    kind: FileType::Directory,
                    perm: 0o444,
                    nlink: 0,
                    uid: self.uid,
                    gid: self.gid,
                    rdev: 0,
                    flags: 0,
                }
            }
        } else {
            if inode_is_root(ino) {
                FileAttr {
                    ino: ino,
                    size: 0,
                    blocks: 0,
                    blksize: 0,
                    atime: SystemTime::UNIX_EPOCH,
                    mtime: SystemTime::UNIX_EPOCH,
                    ctime: SystemTime::UNIX_EPOCH,
                    crtime: SystemTime::UNIX_EPOCH,
                    kind: FileType::Directory,
                    perm: 0,
                    nlink: 0,
                    uid: self.uid,
                    gid: self.gid,
                    rdev: 0,
                    flags: 0,
                }
            } else if inode_is_update(ino) {
                FileAttr {
                    ino: ino,
                    size: 0,
                    blocks: 0,
                    blksize: 0,
                    atime: SystemTime::UNIX_EPOCH,
                    mtime: SystemTime::UNIX_EPOCH,
                    ctime: SystemTime::UNIX_EPOCH,
                    crtime: SystemTime::UNIX_EPOCH,
                    kind: FileType::RegularFile,
                    perm: 0o222,
                    nlink: 0,
                    uid: self.uid,
                    gid: self.gid,
                    rdev: 0,
                    flags: 0,
                }
            } else {
                return Err(ENOENT);
            }
        };
        Ok(attr)
    }

    fn update(&mut self) {
        self.channels.iter_mut().for_each(|c| c.update())
    }
}

impl Filesystem for Rssfs {
    fn init(&mut self, _req: &Request, _: &mut KernelConfig) -> Result<(), i32> {
        println!("Welcome to rssfs");
        Ok(())
    }

    fn destroy(&mut self) {
        println!("Bye bye");
    }

    fn lookup(&mut self, _req: &Request, _parent: u64, _name: &OsStr, reply: ReplyEntry) {
        let ino = if let Some((parent_chan_idx, parent_item_idx)) = indexes_from_inode(_parent) {
            if let Some(_) = parent_item_idx {
                // parent is item => fail
                reply.error(ENOENT);
                return;
            } else {
                // parent is channel directory
                if let Some(i) = self.inode_from_item_name(parent_chan_idx, _name.to_str().unwrap())
                {
                    i
                } else {
                    reply.error(ENOENT);
                    return;
                }
            }
        } else {
            // parent is root
            if _name.to_str().unwrap() == "update" {
                inode_of_update()
            } else {
                if let Some(i) = self.inode_from_channel_name(_name.to_str().unwrap()) {
                    i
                } else {
                    reply.error(ENOENT);
                    return;
                }
            }
        };
        let attr = match self.attr_of_inode(ino) {
            Err(e) => {
                reply.error(e);
                return;
            }
            Ok(s) => s,
        };
        reply.entry(&Duration::from_secs(0), &attr, 0);
    }

    fn open(&mut self, _req: &Request, _ino: u64, _flags: i32, reply: ReplyOpen) {
        println!("call open");
        if let Some((_, Some(_))) = indexes_from_inode(_ino) {
            reply.opened(0, 0);
        } else {
            // Open root or directory
            reply.opened(0, 0);
        }
    }

    fn write(
        &mut self,
        _req: &Request,
        _ino: u64,
        _fh: u64,
        _offset: i64,
        _data: &[u8],
        _write_flags: u32,
        _flags: i32,
        _lock_owner: Option<u64>,
        reply: ReplyWrite,
    ) {
        println!("call write");
        if _ino == inode_of_update() {
            self.update();
            reply.written(_data.len() as u32);
        } else {
            reply.error(EPERM);
        }
    }

    fn opendir(&mut self, _req: &Request, _ino: u64, _flags: i32, reply: ReplyOpen) {
        if let Some((_, item_index)) = indexes_from_inode(_ino) {
            if let Some(_) = item_index {
                // We try to open an item
                reply.error(ENOTDIR);
            } else {
                reply.opened(0, 0);
            }
        } else {
            // Open root
            reply.opened(0, 0);
        }
    }

    fn getattr(&mut self, _req: &Request, _ino: u64, reply: ReplyAttr) {
        match self.attr_of_inode(_ino) {
            Ok(a) => reply.attr(&Duration::from_secs(0), &a),
            Err(e) => reply.error(e),
        }
    }

    fn readdir(
        &mut self,
        _req: &Request,
        _ino: u64,
        _fh: u64,
        _offset: i64,
        mut reply: ReplyDirectory,
    ) {
        if let Some((chan_index, item_index)) = indexes_from_inode(_ino) {
            if let Some(_) = item_index {
                // Try to read an item (which is a file, not a dir)
                reply.error(ENOTDIR);
                return;
            }
            if chan_index as usize >= self.channels.len() {
                reply.error(ENOTDIR);
                return;
            }

            let chan = &self.channels[chan_index as usize];
            let mut i = _offset;

            for (index, item) in chan.clone().items().iter().skip(i as usize) {
                let inode = inode_from_indexes(chan_index, Some(*index as u32));
                if reply.add(
                    inode,
                    i + 1 as i64,
                    FileType::RegularFile,
                    item.title().unwrap_or(&format!("{}", i)),
                ) {
                    break;
                } else {
                    i += 1; //TODO check
                }
            }
        } else {
            // Read root
            let mut i = _offset;
            if _offset == 0 {
                reply.add(
                    inode_of_update(),
                    i + 1 as i64,
                    FileType::RegularFile,
                    "update",
                );
                i += 1;
            }

            for chan in (&self.channels).iter().skip((_offset - 1) as usize) {
                let inode = inode_from_indexes(i as u32, None);
                if reply.add(inode, i + 1 as i64, FileType::Directory, chan.title()) {
                    break;
                } else {
                    i += 1;
                }
            }
        }
        reply.ok()
    }

    fn read(
        &mut self,
        _req: &Request,
        _ino: u64,
        _fh: u64,
        _offset: i64,
        _size: u32,
        _flags: i32,
        _lock_owner: Option<u64>,
        reply: ReplyData,
    ) {
        if let Some((chan_index, Some(item_index))) = indexes_from_inode(_ino) {
            let item = &self.item_at_index(chan_index, item_index).unwrap();
            let msg = string_of_item(item);
            let bytes: Vec<_> = msg
                .bytes()
                .skip(_offset as usize)
                .take(_size as usize)
                .collect();
            reply.data(&bytes);
        } else {
            // Read root or directory
            reply.error(EISDIR)
        }
    }

    fn release(
        &mut self,
        _req: &Request,
        _ino: u64,
        _fh: u64,
        _flags: i32,
        _lock_owner: Option<u64>,
        _flush: bool,
        reply: ReplyEmpty,
    ) {
        reply.ok();
    }

    fn flush(&mut self, _req: &Request, _ino: u64, _fh: u64, _lock_owner: u64, reply: ReplyEmpty) {
        reply.ok();
    }

    fn access(&mut self, _req: &Request, _ino: u64, _mask: i32, reply: ReplyEmpty) {
        reply.ok();
    }
}

fn main() {
    let matches = Command::new("rssfs")
        .version("0.0.1-dev")
        .author("Martin Vassor")
        .about("A filesystem feed aggregator")
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .value_name("FILE")
                .help("Location of the configuration file (~/.config/rssfs by default)")
                .takes_value(true),
        )
        .get_matches();
    let mut home = dirs::home_dir().unwrap();
    home.push(".config/rssfs");
    let home_str = home
        .as_path()
        .as_os_str()
        .to_os_string()
        .into_string()
        .unwrap();

    let config_path = matches.value_of("config").unwrap_or(&home_str);

    let mut file = File::open(config_path).unwrap();

    let mut config_str = String::new();
    file.read_to_string(&mut config_str).unwrap();

    let config: Config = toml::from_str(&config_str).unwrap();

    let uid;
    let gid;
    unsafe {
        uid = libc::getuid();
        gid = libc::getgid();
    }

    let rssfs = Rssfs {
        channels: config
            .channels
            .iter()
            .map(|s| ChanBox::from_url(s))
            .collect(),
        uid,
        gid,
    };
    if let Err(e) = mount2(rssfs, &config.mountpoint, &[]) {
        println!("{}", e);
    }
}
